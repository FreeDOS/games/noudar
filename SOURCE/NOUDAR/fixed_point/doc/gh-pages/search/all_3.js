var searchData=
[
  ['elastic_5ffixed_5fpoint',['elastic_fixed_point',['../namespacesg14.html#a8d711cd1f4de10fc95b8cbdd113ad382',1,'sg14']]],
  ['elastic_5finteger',['elastic_integer',['../classsg14_1_1elastic__integer.html',1,'sg14::elastic_integer&lt; Digits, Narrowest &gt;'],['../classsg14_1_1elastic__integer.html#ad3964a555505969e4a58d90841ce6f2b',1,'sg14::elastic_integer::elastic_integer()=default'],['../classsg14_1_1elastic__integer.html#add56afb9349ff19e869b489382161af6',1,'sg14::elastic_integer::elastic_integer(const elastic_integer &amp;rhs)'],['../classsg14_1_1elastic__integer.html#a23f1ecf0ebff2c43180ee665ea04f476',1,'sg14::elastic_integer::elastic_integer(Number n)'],['../classsg14_1_1elastic__integer.html#ad0bb9d9ddfc8502c216aea2588e7ad93',1,'sg14::elastic_integer::elastic_integer(const elastic_integer&lt; FromWidth, FromNarrowest &gt; &amp;rhs)'],['../classsg14_1_1elastic__integer.html#aa3f1e7a8d6e54b267f4e986345c06737',1,'sg14::elastic_integer::elastic_integer(const_integer&lt; Integral, Value, Digits, Exponent &gt;)']]],
  ['exp2',['exp2',['../namespacesg14.html#a3cdd4cc99979ba17ab2d973410ba2cbc',1,'sg14']]],
  ['exponent',['exponent',['../classsg14_1_1fixed__point.html#aac3836ae1f50c76a9b67e6ab9d744241',1,'sg14::fixed_point']]]
];
