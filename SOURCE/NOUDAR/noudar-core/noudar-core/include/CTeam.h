//
// Created by monty on 04/01/17.
//

#ifndef NOUDAR_CORE_CTEAM_H
#define NOUDAR_CORE_CTEAM_H

namespace Knights {

    enum class ETeam {
        kHeroes, kMonsters, kProps
    };
}

#endif //NOUDAR_CORE_CTEAM_H
